import io
import os
import time
import pytz
import json
import httpx
import base64
import asyncio
import uvicorn
import textwrap
import traceback
import fbaio

from cachetools import TTLCache

from enum import Enum
from collections import namedtuple

from jinja2 import Environment, PackageLoader, select_autoescape

from yarl import URL
from PIL import Image, ImageDraw, ImageFont

from dotenv import load_dotenv
from datetime import datetime as dt

from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse, JSONResponse, FileResponse
from fastapi.routing import APIRouter
from fastapi.templating import Jinja2Templates

from farcaster import Warpcast


load_dotenv()

GUILD_ID = 67432
TOP_ID = 34073

USER_URL = "https://api.guild.xyz/v2/users"
ADD_URL = "https://warpcast.com/~/add-cast-action?url=https://onchain.ceo/api/top"
TOP_URL = f"https://api.guild.xyz/v2/guilds/{GUILD_ID}/points/{TOP_ID}/leaderboard"
POWER_URL = "https://api.warpcast.com/v2/power-badge-users"
IMG_URL = "https://i.imgur.com/K9rdeRI.png"

HUBBLE_URL = "http://38.242.193.129:2281/v1"

TOP_TIMEOUT = int(os.environ.get("TOP_TIMEOUT", 30))
POWER_TIMEOUT = int(os.environ.get("TOP_TIMEOUT", 86400))
REQUEST_TIMEOUT = int(os.environ.get("REQUEST_TIMEOUT", 0.1))

app = FastAPI()
api = APIRouter(prefix="/api")
templates = Jinja2Templates(directory="templates")

top = {
    "data": {},
    "updated": 0,
}

power = {
    "data": set([]),
    "updated": 0,
}

rate_limit = TTLCache(maxsize=1000, ttl=max(TOP_TIMEOUT, REQUEST_TIMEOUT))

wc = Warpcast(access_token=os.environ.get("WC_ACCESS_TOKEN"))

db = fbaio.initialize_app(
    {
        "apiKey": os.environ.get("FB_API_KEY"),
        "databaseURL": os.environ.get("FB_DB_URL"),
        "serviceAccount": json.loads(base64.b64decode(os.getenv("FB_SERVICE_ACCOUNT")).decode("utf-8")),
    }
).database()


Progress = namedtuple("Progress", ["since", "gain", "miss", "roles"])

ButtonT = namedtuple("Button", "text target action")


def Button(text, target=None, action=None):
    return ButtonT(text, target, action)


INDEX = Enum("INDEX", ["INSTALL", "PROGRESS", "STAT"])
PROGRESS = Enum("PROGRESS", ["BACK", "REFRESH", "REBASE"])
STAT = Enum("STAT", ["BACK", "REFRESH"])


def frame(request, **params):
    params["request"] = request

    params["title"] = "Top Onchain"
    params["aspect_ratio"] = params.get("ratio") or "1.91:1"

    if params.get("img"):
        data = base64.b64encode(params["img"].getvalue()).decode("utf-8")
        params["img_url"] = f"data:image/png;base64,{data}"

    return templates.TemplateResponse("frame.html", params, media_type="text/html")


async def get_json(url):
    async with httpx.AsyncClient() as client:
        resp = await client.get(url)
        json = resp.json()
        return json


async def update_top():
    ts = int(time.time())

    if ts - top["updated"] > TOP_TIMEOUT:
        data = await get_json(TOP_URL)

        top["data"] = {
            user["address"]: {"points": user["totalPoints"], "pos": pos}
            for pos, user in enumerate(data.get("leaderboard", {}))
        }

        top["updated"] = ts

 
async def update_power_badges():
    ts = int(time.time())

    if ts - power["updated"] > POWER_TIMEOUT:
        data = await get_json(POWER_URL)

        power["data"] = set(data['result']['fids'])
        power["updated"] = ts


async def update_user(fid, roles, roles_info):
    ts = int(time.time())

    update_user = {
        f"fids/{fid}/roles/": roles,
        f"fids/{fid}/updated/": ts,
    }

    update_roles = {f"roles/{r}/": {"name": "n/a"} for r in roles if str(r) not in roles_info}

    resp = await db.update(update_user | update_roles)
    return ts


async def get_roles_info():
    resp = await db.child("roles").get()
    return resp.val() or {}


async def get_user_roles(address):
    data = await get_json(f"{USER_URL}/{address}/memberships?guildId={GUILD_ID}")
    return (data or {}).get("roleIds")


def request_allowed(fid):
    ts = rate_limit.get(fid, 0)

    if int(time.time()) - ts > REQUEST_TIMEOUT:
        rate_limit[fid] = int(time.time())
        return True 

    return False


def convert_roles(roles_info, user_roles):
    return sorted([roles_info.get(str(r), {}).get("name") or "n/a" for r in user_roles])


def convert_timestamp(ts, tz="utc"):
    return pytz.utc.localize(dt.utcfromtimestamp(ts)).astimezone(pytz.timezone(tz)).isoformat(" ", "seconds")[:-6]


async def progress_refresh(fid):
    progress = Progress(since="", gain=[], miss=[], roles=[])
    action = "welcome"

    try:
        if not request_allowed(fid):
            return "wait", progress

        user = await get_user(fid)
        address = user["address"]

        if not address:
            return "error", progress

        user_roles = await get_user_roles(address)

        if not user_roles:
            return "error", progress

        roles_info = await get_roles_info()

        old_user_roles = set(user.get("roles", []))
        old_user_roles.discard(None)

        if old_user_roles:
            action = address
            progress = Progress(
                since=convert_timestamp(user.get("updated", 0)),
                gain=convert_roles(roles_info, set(user_roles) - old_user_roles),
                miss=convert_roles(roles_info, old_user_roles - set(user_roles)),
                roles=user_roles,
            )
        else:
            await update_user(fid, user_roles, roles_info)

    except Exception as e:
        print(str(e), traceback.format_exc())
        action = "error"

    return action, progress


async def progress_rebase(fid, new_user_roles):
    try:
        if not request_allowed(fid):
            return "wait", None

        user = await get_user(fid)

        if not user.get("address"):
            return "error", progress

        roles_info = await get_roles_info()

        await update_user(fid, new_user_roles, roles_info)
        return None, convert_timestamp(int(time.time()))
    except Exception as e:
        print(str(e), traceback.format_exc())
        return "error", None


async def get_user(fid):
    resp = await db.child("fids").child(fid).get()
    user_data = resp.val()

    if user_data:
        return user_data

    resp = wc.get_verifications(fid)
    for v in resp.dict().get("verifications"):
        address = v.get("address")

        async with httpx.AsyncClient() as client:
            guild_data = await get_json(f"{USER_URL}/{address}")
            user_id = guild_data.get("id")

            if user_id:
                user_data = {"id": user_id, "address": address}
                await db.child("fids").child(fid).set(user_data)
                return user_data

            await asyncio.sleep(0.5)


def get_user_name(fid):
    return wc.get_user(fid).dict()["display_name"]


async def get_top_data(fid):
    user = await get_user(fid)

    if user and user.get("address"):
        data = top["data"].get(user["address"])

        if data:
            return data


def format_data(name, pos, points):
    return f"{name} is top {pos} with {points} points"


def is_big_image(progress):
    return len(progress.gain) > 30 or len(progress.miss) > 30


async def get_request_data(request):
    json_data = await request.json()
    return json_data["untrustedData"]


@api.get("/power-check")
async def get_power_check():
    return {
        "name": "power-check",
        "icon": "zap",
        "description": "user power check",
        "aboutUrl": "https://gitlab.com/the-gethering/top-onchain",
        "action": {"type": "post"},
    }


@api.post("/power-check")
async def post_power_check(request: Request):
    try:
        await update_power_badges()

        json_data = await request.json()
        data = json_data["untrustedData"]

        fid = data["fid"]
        cid = data["castId"]["fid"]

        prefix = "you have"

        if fid != cid:
            fid = cid
            prefix = f"{get_user_name(cid)} has"

        pbs = 0
        pageToken = "first"

        LIMIT = 101

        while pageToken and pbs <= LIMIT:
            url = f"{HUBBLE_URL}/linksByTargetFid?target_fid={fid}&pageSize=10000"

            if pageToken != "first":
                url = f"{url}&pageToken={pageToken}"

            data = await get_json(url)

            followers = set(f["data"]["fid"] for f in data["messages"])
            pbs += len(followers & power["data"])
            pageToken = data["nextPageToken"]
            print(pbs, pageToken)

        if pbs < LIMIT:
            message = f"{prefix} {pbs} power followers"
        else:
            message = f"{prefix} over 100 power followers"

    except Exception as e:
        print(str(e), traceback.format_exc())
        message = "¯\\_(ツ)_/¯"

    return JSONResponse(
        status_code=200,
        content={"message": message},
    )


@api.get("/top")
async def get_top():
    return {
        "name": "top/onchain",
        "icon": "number",
        "description": "find user in the /onchain top",
        "aboutUrl": "https://gitlab.com/the-gethering/top-onchain",
        "action": {"type": "post"},
    }


@api.post("/top")
async def post_top(request: Request):
    try:
        await update_top()

        json_data = await request.json()
        data = json_data["untrustedData"]

        uid = data["fid"]
        cid = data["castId"]["fid"]

        u = await get_top_data(uid)
        c = await get_top_data(cid)

        if u:
            message = f"You're top #{u['pos']} with {u['points']}XP"
        else:
            message = "You're not in the top yet"

        if uid != cid:
            cname = get_user_name(cid)
            message = f"{cname} is not in the top"
            prefix = "but"

            if c:
                message = f"{cname} is #{c['pos']}"
                prefix = "and"

            if u:
                message += f", {prefix} you're #{u['pos']}"

                if c:
                    delta = u["points"] - c["points"]
                    postfix = "ahead" if delta > 0 else "behind"
                    message += f", {abs(delta)}XP {postfix}"
                else:
                    message += f" with {u['points']}XP"

    except Exception as e:
        print(str(e), traceback.format_exc())
        message = "¯\\_(ツ)_/¯"

    return JSONResponse(
        status_code=200,
        content={"message": message},
    )


@app.get("/favicon.ico", include_in_schema=False)
def favicon():
    return FileResponse("./public/favicon.ico")


@app.get("/")
async def index_get(request: Request):
    return await index(request)


@app.post("/")
async def index_post(request: Request):
    data = await get_request_data(request)

    btn = INDEX(data["buttonIndex"])

    if btn == INDEX.PROGRESS:
        return await progress(PROGRESS.REFRESH, data, request)

    if btn == INDEX.STAT:
        return await stat(STAT.REFRESH, data, request)

    return await index(request)


@app.post("/progress")
async def progress_post(request: Request):
    data = await get_request_data(request)
    return await progress(PROGRESS(data["buttonIndex"]), data, request)


@app.post("/stat")
async def stat_post(request: Request):
    data = await get_request_data(request)
    return await stat(STAT(data["buttonIndex"]), data, request)


async def index(request):
    base_url = request.base_url

    return frame(
        request,
        img_url=IMG_URL,
        post_url=base_url,
        buttons=(
            Button("Install Action", "link", ADD_URL),
            Button("Progress"),
            #Button("Stat"),
        ),
    )


async def progress(btn, data, request):

    fid = data["fid"]
    post_url = f"{request.base_url}progress"

    if btn == PROGRESS.BACK:
        return await index(request)

    buttons = [Button("⬅ Back"), Button("Refresh")]

    if btn == PROGRESS.REBASE:
        action, new_date = await progress_rebase(fid, json.loads(data.get("state", "[]")))

        if action == "wait":
            img = wait_img()
        elif action == "error":
            img = error_img()
        else:
            img = rebase_img(new_date)

        return frame(
            request,
            img=img,
            buttons=buttons,
            post_url=post_url,
        )

    if btn == PROGRESS.REFRESH:
        action, progress = await progress_refresh(fid)

        if action == "welcome":
            img = welcome_img()
        elif action == "wait":
            img = wait_img()
        elif action == "error":
            img = error_img()
        else:
            img = progress_img(progress)

        state = None
        if progress.gain or progress.miss:
            buttons.append(Button("Rebase"))
            state = json.dumps(progress.roles)

        ratio = None
        if is_big_image(progress):
            ratio = "1:1"

        return frame(
            request,
            img=img,
            ratio=ratio,
            buttons=buttons,
            state=state,
            post_url=post_url,
        )

    return frame(
        request,
        img=error_img(),
        buttons=buttons,
        post_url=post_url,
    )


async def stat(btn, data, request):

    fid = data["fid"]
    post_url = f"{request.base_url}stat"

    if btn == STAT.BACK:
        return await index(request)

    buttons = [Button("⬅ Back"), Button("Refresh")]

    return frame(
        request,
        img=error_img(),
        buttons=buttons,
        post_url=post_url,
    )


def make_img_buffer(image):
    buffer = io.BytesIO()
    image.save(buffer, "PNG")
    buffer.seek(0)
    return buffer


def error_img():
    img = Image.open("assets/img/bg.png")

    title_font = ImageFont.truetype("assets/fonts/anon_bold.ttf", 40)
    subtitle_font = ImageFont.truetype("assets/fonts/anon.ttf", 25)

    draw = ImageDraw.Draw(img)

    draw.text((150, 160), "Sudden error occurred :(", font=title_font, fill="#d0b7e2")
    draw.text((140, 210), "And it will definitely be fixed shortly!", font=subtitle_font, fill="#d0b7e2")

    return make_img_buffer(img)


def welcome_img():
    img = Image.open("assets/img/bg.png")

    title_font = ImageFont.truetype("assets/fonts/anon_bold.ttf", 40)
    subtitle_font = ImageFont.truetype("assets/fonts/anon.ttf", 25)

    draw = ImageDraw.Draw(img)

    draw.text((100, 160), "Good choice, onchain warior!", font=title_font, fill="#d0b7e2")
    draw.text((75, 210), "Come back later to check your progress since now", font=subtitle_font, fill="#d0b7e2")

    return make_img_buffer(img)


def wait_img():
    img = Image.open("assets/img/bg.png")

    title_font = ImageFont.truetype("assets/fonts/anon_bold.ttf", 40)
    subtitle_font = ImageFont.truetype("assets/fonts/anon.ttf", 25)
    mini_font = ImageFont.truetype("assets/fonts/anon.ttf", 20)

    draw = ImageDraw.Draw(img)

    draw.text((200, 160), "Not so fast, degen!", font=title_font, fill="#d0b7e2")
    draw.text((135, 210), "Better go touch grass for a few moments", font=subtitle_font, fill="#d0b7e2")
    draw.text(
        (35, 365),
        f"For your health and frame performance please request once every {REQUEST_TIMEOUT}s",
        font=mini_font,
        fill="#d0b7e2",
    )

    return make_img_buffer(img)


def rebase_img(new_date):
    img = Image.open("assets/img/bg.png")
    hint_margin = 360

    draw = ImageDraw.Draw(img)

    title_font = ImageFont.truetype("assets/fonts/anon_bold.ttf", 40)
    subtitle_font = ImageFont.truetype("assets/fonts/anon.ttf", 20)

    draw.text((220, 150), "New base date set", font=title_font, fill="#d0b7e2")
    draw.text((300, 200), new_date, font=subtitle_font, fill="#d0b7e2")

    return make_img_buffer(img)


def progress_img(progress):
    if is_big_image(progress):
        img = Image.open("assets/img/bg_sqr.png")
        hint_margin = 760
    else:
        img = Image.open("assets/img/bg.png")
        hint_margin = 360

    draw = ImageDraw.Draw(img)

    title_font = ImageFont.truetype("assets/fonts/anon_bold.ttf", 40)
    subtitle_font = ImageFont.truetype("assets/fonts/anon.ttf", 20)
    stat_font = ImageFont.truetype("assets/fonts/anon_bold.ttf", 20)
    mini_font = ImageFont.truetype("assets/fonts/anon.ttf", 20)
    noto_font = ImageFont.truetype("assets/fonts/noto.ttf", 50)
    noto_font.set_variation_by_name("Bold")

    draw.text((195, 10), "Your progress since", font=title_font, fill="#d0b7e2")
    draw.text((300, 60), progress.since, font=subtitle_font, fill="#d0b7e2")

    if len(progress.gain) > 0 or len(progress.miss) > 0:
        gain_width = draw.textlength("gain", font=subtitle_font)
        miss_width = draw.textlength("miss", font=subtitle_font)

        mid_left = img.width // 4
        mid_right = 3 * img.width // 4

        gain_position = (mid_left - gain_width // 2, hint_margin)
        miss_position = (mid_right - miss_width // 2, hint_margin)

        draw.text(gain_position, "gain", font=subtitle_font, fill="#d0b7e2")
        draw.text(miss_position, "miss", font=subtitle_font, fill="#d0b7e2")

        def chunk_list(lst, n):
            for i in range(0, len(lst), n):
                yield lst[i : i + n]

        y_offset_gain = 100
        y_offset_miss = 100

        for chunk in chunk_list(progress.gain, 6):
            draw.multiline_text((50, y_offset_gain), " ".join(chunk), font=stat_font, fill="#d0b7e2")
            y_offset_gain += 40

        for chunk in chunk_list(progress.miss, 6):
            draw.multiline_text((440, y_offset_miss), " ".join(chunk), font=stat_font, fill="#d0b7e2")
            y_offset_miss += 40

        vertical_line_x = img.width // 2
        draw.line([(vertical_line_x, 90), (vertical_line_x, img.height - 20)], fill="#d0b7e2", width=2)

        horizontal_line_y = hint_margin - 10
        draw.line([(40, horizontal_line_y), (img.width - 40, horizontal_line_y)], fill="#d0b7e2", width=2)
    else:
        draw.text((280, hint_margin // 2), "¯\_(ツ)_/¯", font=noto_font, fill="#d0b7e2")
        draw.text((165, 365), f"No roles changed since you checked last time", font=mini_font, fill="#d0b7e2")

    return make_img_buffer(img)


app.include_router(api)


if __name__ == "__main__":
    uvicorn.run(
        "app:app",
        loop="asyncio",
        host="0.0.0.0",
        port=int(os.environ.get("PORT", 8000)),
        reload=True,
    )
